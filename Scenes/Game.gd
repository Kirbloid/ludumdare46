extends Node2D

var _ending = preload("res://Scenes/Ending.tscn")
var _player_shot = preload("res://Scenes/PlayerShot.tscn")

enum Monster { NORMAL, BIG, FLYING }

var _monster_normal = preload("res://Scenes/MonsterNormal.tscn")
var _monster_big = preload("res://Scenes/MonsterBig.tscn")
var _monster_flying = preload("res://Scenes/MonsterFlying.tscn")

onready var main = get_node("/root/Main")
onready var monsters = get_node("Monsters")

var rng = RandomNumberGenerator.new()

var can_shoot = true
var difficulty_phase = 1
var killed_monsters = 0
var difficulty_increase_limit = 3
var difficulty_increase = 0


func _ready():
	Input.set_default_cursor_shape(Input.CURSOR_CROSS)


func _process(delta):
	if Input.is_action_pressed("click_left") and can_shoot and Input.get_current_cursor_shape() == Control.CURSOR_CROSS:
		var shot = _player_shot.instance()
		shot.position = get_global_mouse_position()
		add_child(shot)
		can_shoot = false
		$ShotDelay.start()


func _dave_death_cutscene():
	can_shoot = false
	$Monsters.queue_free()
	$SpawnMonster.queue_free()
	$Tamagotchi.rect_position = Vector2(435, 42)
	

func dave_dies():
	main.ending = 1
	main.ChangeScene(_ending)


func damage_player():
	$Tamagotchi.damage()


func player_dies():
	main.ending = 2
	$AnimationPlayer.play("Death")


func player_wins():
	main.ending = 3
	$AnimationPlayer.play("Win")

func monster_killed():
	killed_monsters += 1
	difficulty_increase += 1
	if difficulty_increase >= difficulty_increase_limit:
		difficulty_phase += 1
		difficulty_increase_limit += 1
		difficulty_increase = 0


func _on_ShotDelay_timeout():
	can_shoot = true


func _on_SpawnMonster_timeout():
	var monster_to_spawn = rng.randi_range(0,2)
	match monster_to_spawn:
		Monster.NORMAL:
			var x_pos = floor(rng.randf_range(128, 1150))
			var y_pos = 360
			var spawn = _monster_normal.instance()
			spawn.position = Vector2(x_pos, y_pos)
			monsters.add_child(spawn)
		Monster.BIG:
			var x_pos = floor(rng.randf_range(418, 862))
			var y_pos = 360
			var spawn = _monster_big.instance()
			spawn.position = Vector2(x_pos, y_pos)
			monsters.add_child(spawn)
		Monster.FLYING:
			var x_pos = floor(rng.randf_range(130, 1150))
			var y_pos = floor(rng.randf_range(264, 486))
			var spawn = _monster_flying.instance()
			spawn.position = Vector2(x_pos, y_pos)
			monsters.add_child(spawn)

	# time adjust based on difficulty (possible end game check)
	match difficulty_phase:
		1:
			$SpawnMonster.wait_time = 6
		2:
			$SpawnMonster.wait_time = 5.5
		3:
			$SpawnMonster.wait_time = 5
		4:
			$SpawnMonster.wait_time = 4.5
		5:
			$SpawnMonster.wait_time = 4
		6:
			$SpawnMonster.wait_time = 3
		7:
			player_wins()


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Death" or anim_name == "Win":
			main.ChangeScene(_ending)

extends Sprite

onready var _strike = preload("res://Scenes/Slash.tscn")

onready var game = get_node("/root/Main/Game")

export var hitpoints : int

var hitboxes : Array
var current_hitbox = 0


func _ready():
	frame = current_hitbox
	hitboxes.push_back($HitBox1)
	hitboxes.push_back($HitBox2)
	hitboxes.push_back($HitBox3)


func shot():
	hitpoints -= 1
	$AnimationPlayer.play("Shot")
	if hitpoints <= 0:
		game.monster_killed()
		queue_free()


func _on_GetBigger_timeout():
	if current_hitbox < 2:
		hitboxes[current_hitbox].queue_free()
		current_hitbox += 1
		z_index += 1
		if current_hitbox <= 2:
			hitboxes[current_hitbox].show()
			hitboxes[current_hitbox].collision_layer = 2
			hitboxes[current_hitbox].collision_mask = 2
			frame = current_hitbox
	elif current_hitbox == 2:
		var strike = _strike.instance()
		strike.position = position
		get_node("/root/Main/Game").add_child(strike)
		game.damage_player()

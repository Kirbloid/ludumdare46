extends Node2D


func _ready():
	$AnimationPlayer.play("Shot")


func _on_AnimationPlayer_animation_finished(anim_name):
	queue_free()


func _on_HitZone_area_entered(area):
	area.get_parent().shot()

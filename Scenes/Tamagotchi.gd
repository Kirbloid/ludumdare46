extends TextureRect

onready var game = get_node("/root/Main/Game")

onready var food_meter = get_node("DigitalScreen/Meters/Food")
onready var happiness_meter = get_node("DigitalScreen/Meters/Happiness")
onready var life_meter = get_node("DigitalScreen/Meters/Life")

var can_drag = false
var is_dragging = false
var offset = Vector2()

var do_once = true

enum State {
	IDLE,
	MENU,
	FOOD,
	PLAY,
	DEAD
}

var current_state = State.IDLE
var idling_animation = true
var secret_anim = 0

onready var dave = get_node("DigitalScreen/Characters/Dave")

onready var menu = get_node("DigitalScreen/Menu")
var current_menu = 0

# feeding minigame vars
var burger_pos = 0
var feed_dave_pos = 0
var burger_columns = [ -11, 0, 11 ]
var burger_move_delay = 0

# play minigame vars
var goal_pos_x = 0
var goal_pos_y = 0
var play_dave_pos_x = 1
var play_dave_pos_y = 1
var play_columns = [ -11, 0, 11 ]
var play_rows = [ -11, 0, 11 ]

# poo vars
onready var poo = get_node("DigitalScreen/Characters/Poo")
var num_of_poos = 0


func _ready():
	$RandomFlipH.wait_time = game.rng.randf_range(5, 15)


func _process(delta):
	# animations checks
	if happiness_meter.value <= 4 and do_once:
		$AnimationPlayer.play("Sad")
	elif food_meter.value <= 4 and do_once:
		$AnimationPlayer.play("Hungry")
	elif $SleepingTimer.time_left <= 0 and do_once:
		$AnimationPlayer.play("Sleeping")
	elif idling_animation and do_once:
		$AnimationPlayer.play("Idle")
	
	# check for death
	if happiness_meter.value <= 0 or food_meter.value <= 0 and do_once:
		do_once = false
		current_state = State.DEAD
		$DigitalScreen/FeedGame.queue_free()
		$DigitalScreen/PlayGame.queue_free()
		$DigitalScreen/Menu.queue_free()
		$GrabArea.input_pickable = false
		$AnimationPlayer.play("Dead")
		game._dave_death_cutscene()
	
	# mouse inputs
	if Input.is_action_just_pressed("click_left") and can_drag:
		offset = get_global_mouse_position() - rect_position
		is_dragging = true
		
	if Input.is_action_pressed("click_left") and is_dragging:
		var target = get_global_mouse_position() - offset
		if target.x < 0:
			target.x = 0
		if target.x > 870:
			target.x = 870
		if target.y < 0:
			target.y = 0
		if target.y > 115:
			target.y = 115
		
		rect_position = target
			

	if Input.is_action_just_released("click_left") and is_dragging:
		is_dragging = false

	# feeding minigame
	if current_state == State.FOOD:
		feed_game_update()
	
	# playing minigame
	if current_state == State.PLAY:
		play_game_update()


func damage():
	life_meter.value -= 1
	if life_meter.value <= 0:
		game.player_dies()


###### FEED MINIGAME ######

func feed_dave():
	burger_pos = game.rng.randi_range(0,2)
	$DigitalScreen/FeedGame/Burger.position = Vector2(burger_columns[burger_pos], -8)
	feed_dave_pos = burger_pos
	while feed_dave_pos == burger_pos:
		feed_dave_pos = game.rng.randi_range(0,2)
	$DigitalScreen/FeedGame/FeedDave.position = Vector2(burger_columns[feed_dave_pos], 12)
	$DigitalScreen/FeedGame.show()
	current_state = State.FOOD


func feed_game_update():
	if $DigitalScreen/FeedGame/Burger.position.y < 12:
		burger_move_delay += 0.1
		if burger_move_delay >= 1:
			$DigitalScreen/FeedGame/Burger.position.y += 1
			burger_move_delay = 0
	else:
		if feed_dave_pos == burger_pos:
			feed_successful()
		else:
			feed_failed()
		$DigitalScreen/FeedGame.hide()
		current_state = State.IDLE
		$DigitalScreen/Characters.show()
		$SleepingTimer.start()
		idling_animation = false


func feed_successful():
	food_meter.value += 5
	$TimeToPoo.start()
	$AnimationPlayer.play("Happy")

func feed_failed():
	secret_anim += 1
	if secret_anim >= 3:
		$AnimationPlayer.play("Secret")
	else:
		$AnimationPlayer.play("Disappointed")


###### PLAY MINIGAME ######

func play_with_dave():
#OLD SETUP
	#ar map = game.rng.randi_range(0,5)
	#$DigitalScreen/PlayGame/Maze.frame = map
#	match map:
#		0:
#			goal_pos_x = 0
#			goal_pos_y = 0
#			play_dave_pos_x = 1
#			play_dave_pos_y = 1
#		1:
#			goal_pos_x = 2
#			goal_pos_y = 2
#			play_dave_pos_x = 2
#			play_dave_pos_y = 0
#		2:
#			goal_pos_x = 1
#			goal_pos_y = 1
#			play_dave_pos_x = 0
#			play_dave_pos_y = 2
#		3:
#			goal_pos_x = 2
#			goal_pos_y = 0
#			play_dave_pos_x = 0
#			play_dave_pos_y = 2
#		4:
#			goal_pos_x = 0
#			goal_pos_y = 2
#			play_dave_pos_x = 2
#			play_dave_pos_y = 2
#		5:
#			goal_pos_x = 1
#			goal_pos_y = 1
#			play_dave_pos_x = 0
#			play_dave_pos_y = 2

	goal_pos_x = game.rng.randi_range(0,2)
	goal_pos_y = game.rng.randi_range(0,2)
	play_dave_pos_x = goal_pos_x
	play_dave_pos_y = goal_pos_y
	while play_dave_pos_x == goal_pos_x and play_dave_pos_y == goal_pos_y:
		play_dave_pos_x = game.rng.randi_range(0,2)
		play_dave_pos_y = game.rng.randi_range(0,2)
	$DigitalScreen/PlayGame/Goal.position = Vector2(play_columns[goal_pos_x], play_rows[goal_pos_y])
	$DigitalScreen/PlayGame/PlayDave.position = Vector2(play_columns[play_dave_pos_x], play_rows[play_dave_pos_y])
	$DigitalScreen/PlayGame.show()
	current_state = State.PLAY


func play_game_update():
	if play_dave_pos_x == goal_pos_x and play_dave_pos_y == goal_pos_y:
		happiness_meter.value += 5
		$DigitalScreen/PlayGame.hide()
		current_state = State.IDLE
		$DigitalScreen/Characters.show()
		$SleepingTimer.start()
		idling_animation = false
		$AnimationPlayer.play("Happy")


###### POO PATROL ######

func clean_poop():
	num_of_poos = clamp(num_of_poos - 1, 0, 2)
	update_poo_gfx()
	current_state = State.IDLE
	$DigitalScreen/Characters.show()
	idling_animation = false
	$AnimationPlayer.play("Happy")


func update_poo_gfx():
	match num_of_poos:
		0:
			poo.hide()
		1:
			poo.show()
			poo.get_node("AnimationPlayer").play("one_poo")
		2:
			poo.show()
			poo.get_node("AnimationPlayer").play("two_poos")


###### SIGNALS ######


func _on_GrabArea_mouse_entered():
	can_drag = true
	offset = get_global_mouse_position() - rect_position
	Input.set_default_cursor_shape(CURSOR_POINTING_HAND)


func _on_GrabArea_mouse_exited():
	can_drag = false
	Input.set_default_cursor_shape(CURSOR_CROSS)


func _on_MenuButton_button_down():
	match current_state:
		State.IDLE:
			menu.show()
			current_state = State.MENU
			menu.frame = current_menu
			$DigitalScreen/Characters.hide()
		State.MENU:
			match (current_menu):
				0:
					feed_dave()
				1:
					play_with_dave()
				2:
					clean_poop()
			menu.hide()
			current_menu = 0


func _on_UpButton_button_down():
	match current_state:
		State.MENU:
			current_menu = clamp(current_menu - 1, 0, 2)
			menu.frame = current_menu
		State.PLAY:
			play_dave_pos_y = clamp(play_dave_pos_y - 1, 0, 2)
			$DigitalScreen/PlayGame/PlayDave.position = Vector2(play_columns[play_dave_pos_x], play_rows[play_dave_pos_y])


func _on_DownButton_button_down():
	match current_state:
		State.MENU:
			current_menu = clamp(current_menu + 1, 0, 2)
			menu.frame = current_menu
		State.PLAY:
			play_dave_pos_y = clamp(play_dave_pos_y + 1, 0, 2)
			$DigitalScreen/PlayGame/PlayDave.position = Vector2(play_columns[play_dave_pos_x], play_rows[play_dave_pos_y])


func _on_LeftButton_button_down():
	match current_state:
		State.FOOD:
			feed_dave_pos = clamp(feed_dave_pos - 1, 0, 2)
			$DigitalScreen/FeedGame/FeedDave.position = Vector2(burger_columns[feed_dave_pos], 12)
		State.PLAY:
			play_dave_pos_x = clamp(play_dave_pos_x - 1, 0, 2)
			$DigitalScreen/PlayGame/PlayDave.position = Vector2(play_columns[play_dave_pos_x], play_rows[play_dave_pos_y])


func _on_RightButton_button_down():
	match current_state:
		State.FOOD:
			feed_dave_pos = clamp(feed_dave_pos + 1, 0, 2)
			$DigitalScreen/FeedGame/FeedDave.position = Vector2(burger_columns[feed_dave_pos], 12)
		State.PLAY:
			play_dave_pos_x = clamp(play_dave_pos_x + 1, 0, 2)
			$DigitalScreen/PlayGame/PlayDave.position = Vector2(play_columns[play_dave_pos_x], play_rows[play_dave_pos_y])


func _on_FoodGoesDown_timeout():
	food_meter.value -= 1


func _on_HappinessGoesDown_timeout():
	happiness_meter.value -= 1
	$HappinessGoesDown.wait_time = 10
	if food_meter.value <= 5:
		$HappinessGoesDown.wait_time -= 2
	match(num_of_poos):
		1:
			$HappinessGoesDown.wait_time -= 2
		2:
			$HappinessGoesDown.wait_time -= 5


func _on_TimeToPoo_timeout():
	num_of_poos = clamp(num_of_poos + 1, 0, 2)
	update_poo_gfx()


func _on_RandomFlipH_timeout():
	if dave.flip_h:
		dave.flip_h = false
	else:
		dave.flip_h = true
	$RandomFlipH.wait_time = game.rng.randf_range(5, 15)


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Happy" or anim_name == "Disappointed":
		idling_animation = true
	if anim_name == "Dead":
		game.dave_dies()
